# KAVE README #
![Unity Version](https://img.shields.io/badge/Unity%20Version-2017.1.1f1-green.svg)

## What it is ##
The KAVE is a Unity plug-in that:

* Adds parallax effect to Unity projects through Kinect v2 head tracking
* Supports up to 8 screens/projectors in any physical configuration (normal desktop screen, CAVE, etc...)
* Features full skeleton tracking for interaction with virtual objects

The plug-in requires a calibration file with the position, orientation and size of the screens, projectors, projection surfaces and Kinect sensor. For this you can use our  [calibration software](https://bitbucket.org/neurorehablab/kave/downloads/KAVECalibrator.rar).

## Requirements ##
Kinect v2:

* [Sensor with adapter for Windows](https://developer.microsoft.com/en-us/windows/kinect/hardware)
* [Kinect v2 SDK](https://www.microsoft.com/en-us/download/details.aspx?id=44561)
* [Kinect Unity Pro packages](https://developer.microsoft.com/en-us/windows/kinect)

A Unity 2017 project

A calibration file


## How to use it ##
You should use the calibration software to generate a file describing your setup. The file "Calibration.xml" will need to be placed at the "StreamingAssets" folder of your project.

1. Import the official [Kinect Unity package](https://go.microsoft.com/fwlink/p/?LinkId=513177) "Kinect.2.0.1410.19000.unitypackage" into your project
2. Import the [KAVE plug-in](https://bitbucket.org/neurorehablab/kave/downloads/KavePackageKinect_v3.052.unitypackage) into your project
2. Add the VRManager prefab to Hierarchy
3. Game cameras should be disabled as VRManager will create its own
4. Place the "Calibration.xml" that the calibration software generated in the "StreamingAssets" folder of your project.

Optional:

* The location of the VRManager will match the calibration reference frame origin, so it's important where you place it/move it keeping this in mind, vertically it should be placed on the floor of the virtual scene so that the it matches the real world floor.
* By default, the plug-in is set to view only the "default" layer, you can customize which layers to render by changing the culling mask of the prefab "User View Camera" (other visual effects can be added to this prefab like any normal unity camera).

## Using the Calibrator ##
The plug-in needs a calibration file, to generate this file you can use the [calibration software](https://bitbucket.org/neurorehablab/kave/downloads/KAVECalibrator.rar), a calibration guide is included in the archive.

* Controls
	Mid-click:	Pan camera
	ALT + Mid-Click: Orbit camera
	Spacebar:	Select objects
	W:	Translate objects
	E:	Rotate objects
	R:	Scale objects
	X:	Change transform space (Global or Local)

	When an object is selected, the gizmos with selected tool is shown.
	
* Adding objects
	Unlimited surfaces, unlimited projectors and only one sensor is able to be in
	the scene simultaneously. Deleting objects is not allowed, but you can restart the
	scene with the Reset button in the left of the main view.
	
* Save and Load file
	The configuration file is located in "../KaveCalibrator_Data/calibration.xml"
	if you want to load an existing file, you should move the file to the specified path
	with the name "calibration.xml". If you have started a new scene, the saved file will
	be in the same location.

Follow these steps in the following order:
Calibrating surfaces:

1. Press "Surface" to add one
2. Select it
3. Adjust its Position, Rotation and Scale to match your projection screen position orientation and size precisely (in meters). You can use the edit boxes or the tool-bar to manipulate it
4. Go back to step 1 until you have all your room projection surfaces represented

Calibrating projectors:

1. Press "Projector" to add one
2. Select it
3. Adjust its Position and Rotation to roughly match your projector
4. Press “ON/OFF Camera”
5. If the projection is not being displayed on the desired display press “CLOSE VIEW” and change the target display of the projector on its slider to the desired display, you can use the “ON/OFF Camera” to check
6. Once the projector is active on the correct display, press the corresponding number on the keyboard (1 to 8), and use the black circle as mouse cursor to drag the green circles to the corners of your projection surfaces in the real world. The red polygon should completely cover your projection surface. Interaction needs to be done with the black circle as mouse cursor as Unity mouse position is limited for multidisplay
7. Press “Close View”
8. Select the surface that is the target of that projector and also change its display to the same target number as the projector
9. Go back to step 1 until you have all your projectors have been calibrated

Calibrating Kinect:

1. Press “Sensor”
2. Change the its Display to the same as the projector that is projecting on the floor
3. Have someone stand over the projection floor
4. Adjust the Position x and y, and orientation y of the sensor until the feet represented on the projection are directly below the person’s feet

SAVE and put the file "calibration.xml" in the "StreamingAssets" folder of your project.

## License ##
If you use or adapt this software in your research please cite:

	Afonso Gonçalves and Sergi Bermúdez i Badia. 2018.
	KAVE: Building Kinect Based CAVE Automatic Virtual Environments, Methods for Surround-Screen Projection Management, Motion Parallax and Full-Body Interaction Support.
	PACM on Human-Computer Interaction, Vol. 1, EICS, Article 10 (June 2018), 15 pages.
	https://doi.org/10.1145/3229092

The author can be contacted at afonso.goncalves@m-iti.org

Copyright (C) 2017  Afonso Gonçalves 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>